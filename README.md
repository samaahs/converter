<img src="images/IDSN-new-logo.png" width="200"> 

# Hands on lab: Conversion using HTML,CSS & JS

# Objectives

To create a Unit conversion Web page <br>
<img src="FinalImg.jpg"/>

1. Structuring a webpage using HTML Tags
2. Providing design to the webpage using CSS
3. Implement Interactiveness with the help of JavaScript
4. Test your knowledge by adding more features

# Task 1: Structuring a webpage using HTML Tags

1. Open a fresh theia environment and create a new file. Click on File -> New file. Save it by naming it  **index.html**

<img src="images/task-1-1"/>

2. Create a basic structure for HTML in **index.html** with the title of `Conversion`

<img src="images/task-1-2"/>

3. In the body, Create a heading 1 with the text `Conversion`

```
<h1> Conversion </h1>
```

4. Create a div tag with the class name as main
> **Note:** This class name will be used in CSS to provide styling in the next task.

```html
<div class="main">


<div>
```

5. You need to create Unit conversion for:
  <ol type="i">
    <li> Temprature </li>
    <li>  Weight</li>
    <li> Distance </li>
  </ol>

Initially create only for **Temperature**

6. Create a new div tag in within the previously created div tag (main)

Now your code should look like this:
```html
<div class="main">
  <div>

  </div>
<div>
```

7. Now create the following:
  <ol type="a">
    <li> Display <b>Temprature</b> using Heading 2 tag  </li>
    <li>  Create an input field and a label</li>
    <li> Create a span and a button to convert </li>
  </ol>

8. Create a heading 2 tag within the newly created div tag

```html
<div class="main">

  <!-- Converting Temprature from Celcius to Fahrenheit -->
  <div>
    <h2> Temprature </h2>

  </div>
<div>
```
9. Now create Input field, Span and a button.
create these using the following code:

```html
  <div>
    <h2> Temprature </h2>
    <input type="number" id="celcius"> 
    <label for="Temprature">Celcius</label>
    <button> Convert </button>
    <span id="fahrenheit"></span>
  </div>
<div>
```

So far your code should look like this: 
<img src="images/task-1-9"/>

10. Run your application using live server to see if it renders like this

<img src="images/task-1-10"/>

# Task 2: Providing design to the webpage using CSS

1. Link style.css to your index.html using ` <link rel="stylesheet" href="style.css">  ` 
>**Hint:** Include it in the head tag

2. Create a new file (File -> New file). Name it **style.css**

3. In style.css file, horizontally align every text to center using appropriate properties
>**Note:** To select every element in the code use `*{ }` 

```css
*{
    text-align: center;
    margin: 10px;
}
```

4. Give the entire boody a background color of lavender

```css
body{
    background-color: lavender;
}
```

5. Now, give padding of 15px and keep background color white only for h1 and .main class


```css
h1, .main{
    background-color: white;
    padding: 10px;
}
```

6. Here is how your style.css should look like by now: <br>
<img src="images/task-2-6"/> 

6. And your app should be looking like this: <br>
<img src="images/task-2-7"/>

# Task 3: Implement Interactiveness with the help of JavaScript

1. Link script.js to your index.html using ` <script src="script.js"></script> ` 
>**Hint:** Include it in the head tag

2. Create a new file (File -> New file). Name it **script.js**

3. In scrip.js, create a function named temprature.
You can copy paste the code.
> Note: It is a very good practice of writing comments wherver required.

```js
function temprature(){
    //To convert celcius to farenheit
    //(CELCIUS * 9/5) + 32

}
```
4. In the `temprature()`, get the element of id `celcius` and store it in a variable.

```js
    var c = document.getElementById("celcius").value;
```

5. Now, convert celcius to farenheit and store it in the variable.
> Note: to convert celcius to farenheit we use the following formula (CELCIUS * 9/5) + 32

```js
    var f = (c * 9/5) + 32
```

6. Lastly, get the span element with the id `fahrenheit` and pass the converted value of variable f along with a string

```js
    document.getElementById("fahrenheit").innerHTML= f + " Fahrenheit"
```

7. Here is how your script.js should look like by now: <br>
<img src="images/task-3-7"/> 

8. Call this temprature function in the `onclick` event of the button created in the **index.html**

```html
<button onclick="temprature()"> Convert </button>
```

9. Test your app by entering a value in the input box and click convert. It should convert the temprature value to celcius

> Test case: Enter value of 50. After converting, you should get the claue of 122 Fahrenheit.

<img src="images/task-3-9"/> 

# Task 4: Test your knowledge by adding more features

Initially it was mentioned that you need to create Unit conversion for:
  <ol type="i">
    <li> Temprature </li>
    <li>  Weight</li>
    <li> Distance </li>
  </ol>
Out of which so far you have created for Tempreature. You can use that knowlede to implement unit conversion for Weight(KG to Pounds) and Distance(KM to Miles)
<br>
Start building one at a time. Starting with Weight.

> **Note:** Use hint only when you are lost.

### **Weight:** KG to Pounds

1. In **index.html** Create another div tag within the main div.(Just like you did for temprature) and create a heading for weight

2. Create an input field of number type with an id of "kilo" and a label for weight displaying KG
  
3.  Create a span with an id of pound and a button to convert with the onclick event of `weight()`


<details>
  <summary>Click to see how the code in html should look like</summary>

  ```html
  <div>
    <h2>Weight</h2>
    <input type="number" id="kilo"> <label for="Weight">KG</label>   
    <button onclick="weight()"> Convert </button>    
    <span id="pounds"></span>    
  </div>

  ```
</details>

4. Now in **script.js** get the element of id `kilo` and store it in a variable. 

5. Then convert kg to pound by multiplying it with 2.2.

6. Get the span element with the id `pounds` and pass the converted value of the variable along with a string "Pounds"

<details>
  <summary>Click to see how the code should look like</summary>

```js
function weight(){
    //To convert KGs to Pounds
    // KG * 2.2

    var kg = document.getElementById("kilo").value;
    
    var p = kg * 2.2

    document.getElementById("pounds").innerHTML= p + " Pounds"
}
```
</details>

7. Test the application by entering a value in the input box and click convert. It should convert the weight value to pounds

> Test case: Enter value of 5. After converting, you should get the value of 11 Pounds.

<img src="images/task-weight-7"/> 


### **Distance:** KM to Miles

1. In **index.html** Create another div tag within the main div.(Just like you did for temprature and weight) and create a heading for distance

2. Create an input field of number type with an id of "km" and a label for weight displaying KM
  
3.  Create a span with an id of miles and a button to convert with the onclick event of `distance()`


<details>
  <summary>Click to see how the code in html should look like</summary>

  ```html
<div>
  <h2>Distance</h2>
  <input type="number" id="km"> <label for="Distance">KM</label>
  <button onclick="distance()"> Convert </button>
  <span id="miles"></span>
</div>
  ```
</details>
<br>

4. Now in **script.js** get the element of id `km` and store it in a variable. 

5. Then convert kg to pound by multiplying it with 2.2.

6. Get the span element with the id `miles` and pass the converted value of the variable along with a string "Miles"

<details>
  <summary>Click to see how the code should look like</summary>

```js
function distance(){
    //To convert KMs to Miles
    // KM * 0.62137

    var km = document.getElementById("km").value;
    
    var m = km * 0.62137

    document.getElementById("miles").innerHTML= m + " Miles"
}
```
</details>

7. Test the application by entering a value in the input box and click convert. It should convert the distance value to Miles

> Test case: Enter value of 1. After converting, you should get the value of 0.62137 Miles.

This is how your final application should look like

<img src="images/task-weight-7"/> 

Congratulations! You've successfully completed this lab

## Author(s)

Samaah Sarang

## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 14-July-2022 | 1.0 | Samaah | Initial version created |
|   |   |   |   |
